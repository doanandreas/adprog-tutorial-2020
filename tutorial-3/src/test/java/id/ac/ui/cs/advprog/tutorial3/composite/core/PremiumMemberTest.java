package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName() {
        //TODO: Complete me
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member dummy = new OrdinaryMember("Shadow", "Dummy");
        member.addChildMember(dummy);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member dummy = new OrdinaryMember("Shadow", "Dummy");
        member.addChildMember(dummy);
        member.removeChildMember(dummy);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member dummy1 = new OrdinaryMember("Shadow1", "Dummy");
        Member dummy2 = new OrdinaryMember("Shadow2", "Dummy");
        Member dummy3 = new OrdinaryMember("Shadow3", "Dummy");

        member.addChildMember(dummy1);
        member.addChildMember(dummy2);
        member.addChildMember(dummy3);

        assertEquals(3, member.getChildMembers().size());

        Member dummy4 = new OrdinaryMember("Shadow4", "Dummy");
        member.addChildMember(dummy4);

        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        assertEquals("gitoppotig", "gitoppotig");
    }
}
