package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UniqueUpgradeTest {

    private UniqueUpgrade uniqueUpgrade;

    @BeforeEach
    public void setUp(){
        uniqueUpgrade = new UniqueUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals("Shield", uniqueUpgrade.getName());
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        assertEquals("Heater Shield (Unique)", uniqueUpgrade.getDescription());
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        boolean enhanced = false;
        int value = uniqueUpgrade.getWeaponValue();
        if(value >= 20 && value <= 25) enhanced = true;
        assertTrue(enhanced);
    }
}
