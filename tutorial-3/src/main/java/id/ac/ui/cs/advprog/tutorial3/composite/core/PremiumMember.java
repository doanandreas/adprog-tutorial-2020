package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    //TODO: Complete me
    String name;
    String role;
    List<Member> childList;

    public PremiumMember(String name, String role) {
        this.name = name;
        this.role = role;
        this.childList = new ArrayList<>();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return this.role;
    }

    @Override
    public void addChildMember(Member member) {
        if(this.childList.size() < 3 || this.role == "Master") {
            this.childList.add(member);
        }
    }

    @Override
    public void removeChildMember(Member member) {
        this.childList.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return this.childList;
    }
}
