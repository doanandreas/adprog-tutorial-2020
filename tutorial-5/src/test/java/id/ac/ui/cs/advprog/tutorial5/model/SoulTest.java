package id.ac.ui.cs.advprog.tutorial5.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SoulTest {
    private Soul soul;

    @BeforeEach
    public void setUp(){
        soul = new Soul(1, "Shadow", 19, "?", "Dummy");
    }

    @Test
    void getGender() {
        assertEquals("?", soul.getGender());
    }

    @Test
    void setGender() {
        soul.setGender("M");
        assertEquals("M", soul.getGender());
    }

    @Test
    void getOccupation() {
        assertEquals("Dummy", soul.getOccupation());
    }

    @Test
    void setOccupation() {
        soul.setOccupation("Merchant");
        assertEquals("Merchant", soul.getOccupation());
    }

    @Test
    void getName() {
        assertEquals("Shadow", soul.getName());
    }

    @Test
    void setName() {
        soul.setName("Emil");
        assertEquals("Emil", soul.getName());
    }

    @Test
    void getAge() {
        assertEquals(19, soul.getAge());
    }

    @Test
    void setAge() {
        soul.setAge(400);
        assertEquals(400, soul.getAge());
    }

    @Test
    void getID() {
        assertEquals(1, soul.getId());
    }
}
