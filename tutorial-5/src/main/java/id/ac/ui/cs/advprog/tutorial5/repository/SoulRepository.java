package id.ac.ui.cs.advprog.tutorial5.repository;

import id.ac.ui.cs.advprog.tutorial5.model.Soul;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SoulRepository extends JpaRepository<Soul, Long> {
}

