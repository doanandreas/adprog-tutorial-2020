package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.model.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SoulServiceImpl implements SoulService {

    @Autowired
    private SoulRepository repository;

    public SoulServiceImpl(SoulRepository repo){
        this.repository = repo;
    }

    @Override
    public List<Soul> findAll() {
        return repository.findAll();
    }

    @Override
    public Optional<Soul> findSoul(Long id) {
        Soul soul = repository.getOne(id);
        Optional<Soul> opt = Optional.ofNullable(soul);
        return opt;
    }

    @Override //delete
    public void erase(Long id) {
        repository.deleteById(id);
    }

    @Override //update
    public Soul rewrite(Soul soul) {
        repository.save(soul);
        return soul;
    }

    @Override //create
    public Soul register(Soul soul) {
        return repository.save(soul);
    }
}
