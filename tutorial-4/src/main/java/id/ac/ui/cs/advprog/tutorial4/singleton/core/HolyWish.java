package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {

    private String wish;

    // TODO complete me with any Singleton approach
    private static HolyWish instance;

    private HolyWish() {
        wish="";
    }

    public static HolyWish getInstance() {
        if(instance == null){
            instance = new HolyWish();
        }

        return instance;
    }

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }
}
