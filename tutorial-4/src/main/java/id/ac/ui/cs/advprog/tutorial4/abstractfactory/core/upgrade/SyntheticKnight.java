package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class SyntheticKnight extends Knight {
    public SyntheticKnight(Armory armory) {
        super();
        this.armory = armory;
        this.desc = "veri synthetic";
    }

    @Override
    public void prepare() {
        // TODO complete me
        this.weapon = armory.craftWeapon();
        this.skill = armory.learnSkill();
    }
}
