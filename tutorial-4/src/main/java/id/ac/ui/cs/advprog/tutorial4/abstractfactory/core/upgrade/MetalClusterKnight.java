package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class MetalClusterKnight extends Knight {
    public MetalClusterKnight(Armory armory) {
        super();
        this.armory = armory;
        this.desc = "veri metal";
    }

    @Override
    public void prepare() {
        // TODO complete me
        this.armor = armory.craftArmor();
        this.skill = armory.learnSkill();
    }
}
