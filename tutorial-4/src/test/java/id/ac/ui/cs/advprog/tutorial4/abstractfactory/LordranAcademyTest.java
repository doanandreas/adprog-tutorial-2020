package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LordranAcademyTest {
    KnightAcademy lordranAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        lordranAcademy = new LordranAcademy();
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        majesticKnight = lordranAcademy.getKnight("majestic");
        metalClusterKnight = lordranAcademy.getKnight("metal cluster");
        syntheticKnight = lordranAcademy.getKnight("synthetic");

        assertNotNull(majesticKnight);
        assertNotNull(metalClusterKnight);
        assertNotNull(syntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        majesticKnight = lordranAcademy.getKnight("majestic");
        metalClusterKnight = lordranAcademy.getKnight("metal cluster");
        syntheticKnight = lordranAcademy.getKnight("synthetic");

        assertEquals("Lordran Majestic Knight", majesticKnight.getName());
        assertEquals("Lordran Metal Cluster Knight", metalClusterKnight.getName());
        assertEquals("Lordran Synthetic Knight", syntheticKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        majesticKnight = lordranAcademy.getKnight("majestic");
        metalClusterKnight = lordranAcademy.getKnight("metal cluster");
        syntheticKnight = lordranAcademy.getKnight("synthetic");

        assertEquals("veri majestic", majesticKnight.getDesc());
        assertEquals("veri metal", metalClusterKnight.getDesc());
        assertEquals("veri synthetic", syntheticKnight.getDesc());
    }
}
