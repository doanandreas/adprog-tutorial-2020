package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    private HolyGrail holyGrail;

    // TODO create tests
    @BeforeEach
    public void setUp(){
        holyGrail = new HolyGrail();
    }

    @Test
    public void testGetHolyWishNotReturnNull(){
        assertNotNull(holyGrail.getHolyWish());
    }

    @Test
    public void testMakeAWishIsWorking(){
        holyGrail = spy(holyGrail);
        holyGrail.makeAWish("");

        verify(holyGrail, atLeastOnce()).makeAWish("");

    }
}
