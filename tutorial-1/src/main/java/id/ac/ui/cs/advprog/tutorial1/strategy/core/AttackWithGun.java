package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
        //ToDo: Complete me

    @Override
    public String attack() {
        return "M4A1 Attack";
    }

    @Override
    public String getType() {
        return "AttackWithGun";
    }
}
