package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
        //ToDo: Complete me
    public MysticAdventurer(){
        super();
        setAttackBehavior(new AttackWithMagic());
        setDefenseBehavior(new DefendWithShield());
    }

    @Override
    public String getAlias() {
        return "I am mystic";
    }
}
