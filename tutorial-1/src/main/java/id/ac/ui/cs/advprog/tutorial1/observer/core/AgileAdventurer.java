package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                //ToDo: Complete Me
                this.guild = guild;
                this.guild.add(this);
        }

        //ToDo: Complete Me

        @Override
        public void update() {
                String qC = guild.getQuestType();

                if(qC.equals("D") || qC.equals("R")) {
                        this.getQuests().add(guild.getQuest());
                }
        }
}
